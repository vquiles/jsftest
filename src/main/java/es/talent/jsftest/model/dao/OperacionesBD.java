package es.talent.jsftest.model.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import es.talent.jsftest.model.entity.Alumno;

public class OperacionesBD {
	private String DB_URL = "jdbc:mysql://localhost/colegio";
	private String USER = "root";
	private String PASS = "root";

	public List<Alumno> obtenerAlumnos() throws SQLException {
		Connection conn = this.crearConexion();
		Statement stmt = null;
		ResultSet rs = null;
		String sql;

		sql = "SELECT id, first, last, age FROM Employees";
		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				int id = rs.getInt("id");
				int age = rs.getInt("age");
				String first = rs.getString("first");
				String last = rs.getString("last");
			}
		} catch (SQLException ex) {

		} finally {
			rs.close();
			if (stmt != null) {
				stmt.close();
			}
			this.cerrarConexion(conn);
		}
		return null;
	}

	public void insertarAlumno(Alumno alumno) throws SQLException {
		Connection conn = this.crearConexion();
		Statement stmt = null;
		String sql;

		sql = "insert into alumno (nombre, apellido, edad, sexo, dni) values " + "('" + alumno.getNombre() + "','"
				+ alumno.getApellido() + "'," + alumno.getEdad() + ",'" + alumno.getSexo().getInicial() + "','"
				+ alumno.getDni() + "')";
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			this.cerrarConexion(conn);
		}
	}

	private void cerrarConexion(Connection conn) throws SQLException {
		if (conn != null) {
			conn.close();
		}
	}

	private Connection crearConexion() {
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (SQLException se) {
			se.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
}
