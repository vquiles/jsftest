package es.talent.jsftest.model.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import es.talent.jsftest.model.enums.Sexo;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Entity
@NoArgsConstructor
@ToString
public class Alumno {

	@Id
	@GeneratedValue
	private Integer id;
	private String nombre;
	private String apellido;
	private String dni;
	private int edad;
	private Sexo sexo;

}
