package es.talent.jsftest.model.enums;

import lombok.Getter;
import lombok.Setter;

public enum Sexo {
	MASCULINO("M"), FEMENINO("F");

	@Getter
	@Setter
	private String inicial;

	private Sexo(String ini) {
		this.inicial = ini;
	}
	
	public static Sexo getSexoPorInicial(String ini) {
		Sexo salida = null;
		if (Sexo.MASCULINO.getInicial().equals(ini)) {
			salida = Sexo.MASCULINO;
		} else if (Sexo.FEMENINO.getInicial().equals(ini)) {
			salida = Sexo.FEMENINO;
		}
		return salida;
	}

}






