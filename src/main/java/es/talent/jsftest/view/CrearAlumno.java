package es.talent.jsftest.view;

import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import es.talent.jsftest.model.dao.HibernateUtils;
import es.talent.jsftest.model.dao.OperacionesBD;
import es.talent.jsftest.model.entity.Alumno;
import es.talent.jsftest.model.enums.Sexo;
import lombok.Getter;
import lombok.Setter;

@ManagedBean
@ViewScoped
@Getter
@Setter
public class CrearAlumno {

	@PersistenceContext( unitName = "colegio")
	private EntityManager em;
	
	private String nombre;
	private String apellidos;
	private String dni;
	private int edad;
	private String sexo;
	
	@PostConstruct
	public void init() {
		Alumno alumno = new Alumno();
		alumno.setNombre("asdad");
		alumno.setApellido("asdas");
		alumno.setEdad(32);
		alumno.setDni("asasd");
		alumno.setSexo(Sexo.MASCULINO);
		
        Transaction transaction = null;
        this.em.persist(alumno);
        try (Session session = HibernateUtils.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(alumno);
            Query query = session.createQuery("select a from Alumno where nomrbe = :nombre");
            query.setParameter("idAlumno", "Vicente");
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
		this.sexo = "M";
	}

	public String crear() throws SQLException {
		OperacionesBD bd = new OperacionesBD();
		Alumno alumno = new Alumno();
		alumno.setNombre(this.nombre);
		alumno.setApellido(this.apellidos);
		alumno.setEdad(this.edad);
		alumno.setDni(this.dni);
		alumno.setSexo(Sexo.getSexoPorInicial(this.sexo));

		bd.insertarAlumno(alumno);
		
		return "/index.xhtml";
	}

}



















