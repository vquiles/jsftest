package es.talent.jsftest.view;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

@ManagedBean
@SessionScoped
public class IndexBean {
	public String irA(String direccion) {
		String salida = null;
		if ("crearAlumno".equals(direccion)) {
			salida = "alumno/crearAlumno.xhtml";			
		}
		return salida;
	}
}
